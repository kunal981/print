//
//  CheckInEventViewController.swift
//  Printer
//
//  Created by mrinal khullar on 9/23/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CheckInEventViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {
    
    var count = Int()
    @IBOutlet var eventNameLbl: UILabel!
    var eventNameStr = NSString()
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var vieww: UIView!
    @IBOutlet var checkInBtn: UIButton!
    var groupIdBoll = Bool()
    var grahamBool = Bool()
    var jennaBool = Bool()
    var groupIdStr = String()
    var groupIdArray = [String]()
    var joinedString = String()
    
    var strId = [String]()
    @IBOutlet var checkInListTableView: UITableView!
    
    @IBOutlet var checkEventTxtField: UITextField!
    
    var userNameArray = NSMutableArray()
    var arrayBool = [Bool]()
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
       self.navigationController?.navigationBarHidden = false
        
        indicator.hidden = true
         indicator.frame = CGRectMake(((view.frame.width/2)-(indicator.frame.width/2)) , ((view.frame.height/2)-(indicator.frame.height/2)), indicator.frame.width, indicator.frame.height)
        
        eventNameLbl.text = eventNameStr as String
        
        if groupIdBoll == false
        {
            
            groupIdArray = [groupIdStr]
            
            var joiner = ","
            joinedString = joiner.join(groupIdArray)
            
            
            println(joinedString)

            
            println("fals = \(groupIdArray)")
        }
        else
        {
            println(groupIdArray)
            
            var joiner = ","
            joinedString = joiner.join(groupIdArray)
            
            
            println(joinedString)
            
        }
        
        
        
        
        let paddingViewCheckEvent = UIView(frame: CGRectMake(0, 0, 15, checkEventTxtField.frame.size.height))
        checkEventTxtField.leftView = paddingViewCheckEvent
        checkEventTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        checkEventTxtField.delegate = self
        
       
    }
    override func viewWillAppear(animated: Bool)
    {
        strId = []
        userNameArray = []
        arrayBool = [false]
        checkInListTableView.reloadData()
        checkInBtn.hidden = true
        checkEventTxtField.text = ""
        count = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //Mark:- TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        checkEventTxtField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if checkEventTxtField.text == ""
        {
           
            userNameArray = []
            self.checkInListTableView.reloadData()
            
        }
        else
        {
            var checkEventStr = checkEventTxtField.text.stringByReplacingOccurrencesOfString("-", withString: "", options: nil, range: nil)
            println(checkEventStr)
            
            self.indicator.hidden = false
            indicator.startAnimating()
            
           
            var dataModel = NSData()
            
            var action = "search"
            
            var post = NSString(format:"action=%@&groupid=%@&field=%@",action,joinedString,checkEventStr)
            
            println(post)
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(dataModel.length)
            
            var url = NSURL(string: "http://crowd.tools/api/loginapi.php?")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                if Reachability.isConnectedToNetwork() == true
                {
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    
                    
                    println(dicObj)
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data Nil", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        self.indicator.stopAnimating()
                        self.indicator.hidden = true
                        

                    }
                    else
                    {
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var status = dicObj?.valueForKey("status") as! String
                            
                            
                            if status == "success"
                            {
                                
                                self.indicator.stopAnimating()
                                self.indicator.hidden = true
                                self.userNameArray = dicObj?.valueForKey("data") as! NSMutableArray
                                self.checkInListTableView.reloadData()
                                
                                for var i = 0; i < self.userNameArray.count; i++
                                {
                                    self.arrayBool.append(false)
                                    
                                }
                                
                            }
                                
                            else
                            {
                                self.indicator.stopAnimating()
                                self.indicator.hidden = true
                                let message = dicObj?.valueForKey("message") as! String
                                self.userNameArray = []
                                self.checkInListTableView.reloadData()
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                                
                            }
                        })
                        
                    }
                }
                }
                
                else
                {
                    
                    self.indicator.stopAnimating()
                    self.indicator.hidden = true
                    var alert = UIAlertView(title: "Alert", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                }
                
            })
            task.resume()
            
        }
    }
    
    
    
    
    //MARK:- TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
    {
        return userNameArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("checkInEventCell") as? CheckInEventTableViewCell
        
        let row = indexPath.row


        
        println(indexPath.row)
        
        
        if arrayBool[indexPath.row] == false
        {
            
            cell!.btn.setBackgroundImage(UIImage(named: "selection bubble.png"), forState: UIControlState.Normal)
            
        }
        else
        {
            cell!.btn.setBackgroundImage(UIImage(named: "selected bubble.png"), forState: UIControlState.Normal)
            
        }
        
     

       cell!.btn.tag = row



        var strName = ""
        
        if let name = userNameArray[row].valueForKey("name") as? String
        {
            strName = name
        }
        
        var strLastName = ""
        
        if let lastName = userNameArray[row].valueForKey("lname") as? String
        {
            strLastName = lastName
        }
        
         var  strNameAndLname = NSString(format: "%@ %@", strName,strLastName)
        
        cell?.userLbl.text = strNameAndLname as String
        
         return cell!
        
        
    }
    
    @IBAction  func btnAction(sender: UIButton)
    {
        if count < 4
        {
            
        
        
       // println("working")
        println(sender.tag)
        checkInBtn.hidden = true
        if arrayBool[sender.tag] == false
        {
            
            sender.setBackgroundImage(UIImage(named: "selected bubble.png"), forState: UIControlState.Normal)
            arrayBool[sender.tag] = true
            checkInBtn.hidden = false
            count++
            
        }
        else
        {
             sender.setBackgroundImage(UIImage(named: "selection bubble.png"), forState: UIControlState.Normal)
             arrayBool[sender.tag] = false
             checkInBtn.hidden = true
            
            for var i = 0; i < arrayBool.count; i++
            {
                if arrayBool[i] == true
                {
                    checkInBtn.hidden = false
                }
                
            }
            
            count--
        }
        }
        
        else if arrayBool[sender.tag] == true
        {
            
            sender.setBackgroundImage(UIImage(named: "selection bubble.png"), forState: UIControlState.Normal)
            arrayBool[sender.tag] = false
            checkInBtn.hidden = true
            
            for var i = 0; i < arrayBool.count; i++
            {
                if arrayBool[i] == true
                {
                    checkInBtn.hidden = false
                }
                
            }
            
            count--

            
        }
        else
        {
            var alert = UIAlertView(title: "Alert", message: "You can only select maximum 4 names.", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    
    //MARK:- Check In Button
    
    
    @IBAction func checkInBtn(sender: AnyObject)
    {
        
        
        
        strId = []
        for var i = 0; i < userNameArray.count; i++
        {
            if arrayBool[i] == true
            {
                strId.append(userNameArray[i].valueForKey("id") as! String)
            }
        }
        
        
        var welcome = storyboard?.instantiateViewControllerWithIdentifier("welcome") as! WelcomeViewController
        welcome.userStriD = strId
        welcome.eventNameStr = eventNameLbl.text!
        self.presentViewController(welcome, animated: false, completion: nil)

        
        
        
        
        

    }
    
    //MARK:- Back Button
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
}

