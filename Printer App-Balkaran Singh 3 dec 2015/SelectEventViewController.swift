//
//  SelectEventViewController.swift
//  Printer
//
//  Created by mrinal khullar on 9/23/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SelectEventViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    var eventArray = NSMutableArray()
    
    @IBOutlet var eventTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.indicator.hidden = true
        indicator.frame = CGRectMake(((view.frame.width/2)-(indicator.frame.width/2)) , ((view.frame.height/2)-(indicator.frame.height/2)), indicator.frame.width, indicator.frame.height)
        
        
        eventsApi()
        
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
           self.navigationController?.navigationBarHidden = true
    }
    func eventsApi()
    {
        self.indicator.hidden = false
        indicator.startAnimating()
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        let orgId = userDefaults.valueForKey("orgId") as! String
        
        var dataModel = NSData()
        
        var action = "eventlisting"
        var post = NSString(format:"action=%@&orgid=%@",action,orgId)
        
        println(post)
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://crowd.tools/api/loginapi.php?")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if Reachability.isConnectedToNetwork() == true
            {
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    if data == nil
                    {
                        var alert = UIAlertView(title: "Alert", message: "Events Not Found", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                    }
                    else
                    {
                        var error:NSError?
                        
                        var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                        
                        
                        println(dicObj)
                        
                        
                        if (error != nil)
                        {
                            println("\(error?.localizedDescription)")
                            var alert = UIAlertView(title: "Alert", message: "Data Nil", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            self.indicator.stopAnimating()
                            self.indicator.hidden = true

                        }
                        else
                        {
                            
                            
                            
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                self.eventArray = dicObj?.valueForKey("data") as! NSMutableArray
                                self.eventTableView.reloadData()
                                self.indicator.stopAnimating()
                                self.indicator.hidden = true
                                
                            })
                            
                        }
                    }
                }
            }
            else
            {
                var alert = UIAlertView(title: "Alert", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                self.indicator.stopAnimating()
                self.indicator.hidden = true
            }
            
        })
        task.resume()
        
        
        
        
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
    {
        return eventArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("eventCell", forIndexPath: indexPath) as! SelectEventTableViewCell
        
        var strTitle = ""
        
        if let title = eventArray[indexPath.row].valueForKey("title") as? String
        {
            strTitle = title
        }
        
        var strTimeStamp = ""
        
        if let timeStamp = eventArray[indexPath.row].valueForKey("timestamp") as? String
        {
            let dateAsString = timeStamp
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let date = dateFormatter.dateFromString(dateAsString)
            
            dateFormatter.dateFormat = "h:mm a"
            let date12 = dateFormatter.stringFromDate(date!)
            
            strTimeStamp = date12
        }
        
        var titleAndTimeStamp = NSString(format: "%@ %@", strTitle,strTimeStamp)
        
        
        cell.eventLbl.text = titleAndTimeStamp as String
        
        
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let checkInEvent = storyboard?.instantiateViewControllerWithIdentifier("checkInEvent") as! CheckInEventViewController
        
        checkInEvent.groupIdStr = eventArray[indexPath.row].valueForKey("groupid") as! String
        checkInEvent.groupIdBoll = false
        
        var strTitle = ""
        
        if let title = eventArray[indexPath.row].valueForKey("title") as? String
        {
            strTitle = title
        }
        
        var strTimeStamp = ""
        
        if let timeStamp = eventArray[indexPath.row].valueForKey("timestamp") as? String
        {
            let dateAsString = timeStamp
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let date = dateFormatter.dateFromString(dateAsString)
            
            dateFormatter.dateFormat = "h:mm a"
            let date12 = dateFormatter.stringFromDate(date!)
            
            strTimeStamp = date12

            
            
        }
        
        var titleAndTimeStamp = NSString(format: "Check in for ''%@ %@''", strTitle,strTimeStamp)
        
        checkInEvent.eventNameStr = titleAndTimeStamp

        self.navigationController?.pushViewController(checkInEvent, animated: false)
        
    }
    
    //MARK:- All Event Button
    
    @IBAction func allEventButton(sender: AnyObject)
    {
        let checkInEvent = storyboard?.instantiateViewControllerWithIdentifier("checkInEvent") as! CheckInEventViewController
        
        checkInEvent.groupIdArray = eventArray.valueForKey("groupid") as! [String]
        checkInEvent.groupIdBoll = true
        println(checkInEvent.groupIdArray)
        checkInEvent.eventNameStr = "Check in for ''ALL EVENTS TODAY''"
        self.navigationController?.pushViewController(checkInEvent, animated: false)
    }
}
