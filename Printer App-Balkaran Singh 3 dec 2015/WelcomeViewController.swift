//
//  WelcomeViewController.swift
//  Printer
//
//  Created by mrinal khullar on 10/1/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet var printTableView: UITableView!
    @IBOutlet var subview: UIView!
    @IBOutlet var eventNameLbl: UILabel!
    var eventNameStr = NSString()
    @IBOutlet var indicator: UIActivityIndicatorView!
    var joinedString = String()
    var userNameArray = NSArray()
    var userStriD = [String]()
    @IBOutlet var welcomeTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventNameLbl.text = eventNameStr as String
        self.indicator.hidden = true
        indicator.frame = CGRectMake(((view.frame.width/2)-(indicator.frame.width/2)) , ((view.frame.height/2)-(indicator.frame.height/2)), indicator.frame.width, indicator.frame.height)
        println(userStriD)
        
        var joiner = ","
        joinedString = joiner.join(userStriD)

        println(joinedString)
        welomeApi()
        
        
        
       

       
    }
    
    func printView()
    {
       
        let printInfo1 = UIPrintInfo(dictionary: nil)
        
        printInfo1.outputType = UIPrintInfoOutputType.General
        printInfo1.jobName = "print job"
        
        let printController = UIPrintInteractionController.sharedPrintController()
        
        printController?.printInfo = printInfo1
        printController?.showsPaperSelectionForLoadedPapers = true
        
        let screenshot = subview.screenshot
        printController?.printingItem = screenshot
        
        
        
        
        printController?.presentAnimated(true, completionHandler: nil)
        
        
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
        func welomeApi()
        {
            self.indicator.hidden = false
            indicator.startAnimating()

        
        var dataModel = NSData()
        
        var action = "checkin"
        
        var post = NSString(format:"action=%@&userid=%@",action,joinedString)
        
        println(post)
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength = String(dataModel.length)
        
        var url = NSURL(string: "http://crowd.tools/api/loginapi.php?")
        
        var urlRequest = NSMutableURLRequest(URL: url!)
        
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if Reachability.isConnectedToNetwork() == true
            {
                
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                    var error:NSError?
                    var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                    
                    
                    println(dicObj)
                    
                    
                    if (error != nil)
                    {
                        println("\(error?.localizedDescription)")
                        var alert = UIAlertView(title: "Alert", message: "Data Nil", delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        self.indicator.stopAnimating()
                        self.indicator.hidden = true
                        

                    }
                    else
                    {
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var status = dicObj?.valueForKey("status") as! String
                            
                            
                            if status == "success"
                            {
                                
                                self.indicator.stopAnimating()
                                self.indicator.hidden = true

                                self.userNameArray = dicObj?.valueForKey("data") as! NSMutableArray
                                println(self.userNameArray)
                                self.welcomeTableView.reloadData()
                                self.printTableView.reloadData()
                                
                                
                                self.printView()
                            }
                                
                            else
                            {
                                self.indicator.stopAnimating()
                                self.indicator.hidden = true

                                let message = dicObj?.valueForKey("message") as! String
                                self.userNameArray = []
                                self.welcomeTableView.reloadData()
                                self.printTableView.reloadData()
                                var alert = UIAlertView(title: "Alert", message: message, delegate: self, cancelButtonTitle: "OK")
                                alert.show()
                                
                                
                            }
                        })
                        
                    }
                }
            }
                
            else
            {
                
                self.indicator.stopAnimating()
                self.indicator.hidden = true

                var alert = UIAlertView(title: "Alert", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
        })
        task.resume()

    }
    
    //MARK:- TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        
    {
        
              return userNameArray.count
        
    
    
        
      
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView == welcomeTableView
        {
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("welcomeCell") as? WelcomeTableViewCell
        
        let row = indexPath.row
        
        
        
        var strName = ""
        
        if let name = userNameArray[row].valueForKey("name") as? String
        {
            strName = name
        }
        
        var strLastName = ""
        
        if let lastName = userNameArray[row].valueForKey("lname") as? String
        {
            strLastName = lastName
        }
        
        
        var  strNameAndLname = NSString(format: "%@ %@", strName,strLastName)
        
        
        cell?.userLbl.text = strNameAndLname as String
            
        
        
        
        
        return cell!
        }
        else
        {
            var printCell = tableView.dequeueReusableCellWithIdentifier("printCell", forIndexPath: indexPath) as! WelcomeTableViewCell
            
            printCell.nameLbl.text = ""
            
            if let name = userNameArray[indexPath.row].valueForKey("name") as? String
            {
                printCell.nameLbl.text = name
            }
            
            printCell.lastNameLbl.text = ""
            
            if let lastname = userNameArray[indexPath.row].valueForKey("lname") as? String
            {
                printCell.lastNameLbl.text = lastname
            }
            
            printCell.dateOfBirth.text = ""
            
            if let dateofbirth = userNameArray[indexPath.row].valueForKey("bday") as? String
            {
                let dateAsString = dateofbirth
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MMMM dd, YYYY"
                let date = dateFormatter.dateFromString(dateAsString)
                println(date)
                
                
                dateFormatter.dateFormat = "MMM dd, YYYY"
                let date12 = dateFormatter.stringFromDate(date!)
                println(date12)

                
                
                printCell.dateOfBirth.text = date12
            }
            
            printCell.checkedInTimeStamp.text = ""
            
            if let timeStamp = userNameArray[indexPath.row].valueForKey("timestamp") as? String
            {
                let dateAsString = timeStamp
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-DD HH:mm:ss"
                let date = dateFormatter.dateFromString(dateAsString)
                println(date)
                
                
                dateFormatter.dateFormat = "hh:mma MMM dd, YYYY"
                let date12 = dateFormatter.stringFromDate(date!)
                println(date12)
                
                printCell.checkedInTimeStamp.text = NSString(format: "Checked in: %@", date12) as String
               
            }
            


            
            return printCell
        }
        
        
    }
    
//MARK:- Welcome Button
    
    @IBAction func welcomeBtn(sender: AnyObject)
    {
        
        self.dismissViewControllerAnimated(false, completion: nil)
    }

    
}

extension UIView{
    
    var screenshot: UIImage{
        
        UIGraphicsBeginImageContext(self.bounds.size);
        let context = UIGraphicsGetCurrentContext();
        self.layer.renderInContext(context)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return screenShot
    }
}

