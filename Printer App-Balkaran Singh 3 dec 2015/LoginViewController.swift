//
//  LoginViewController.swift
//  Printer
//
//  Created by mrinal khullar on 9/23/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var wrongpwdTxtField: UITextField!
    @IBOutlet var wrongUserTxtField: UITextField!
    @IBOutlet var userNameTxtField: UITextField!
    @IBOutlet var pwdTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        
        self.indicator.hidden = true
        
        let paddingViewUserName = UIView(frame: CGRectMake(0, 0, 15, userNameTxtField.frame.size.height))
        userNameTxtField.leftView = paddingViewUserName
        userNameTxtField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewPwd = UIView(frame: CGRectMake(0, 0, 15, pwdTxtField.frame.size.height))
        pwdTxtField.leftView = paddingViewPwd
        pwdTxtField.leftViewMode = UITextFieldViewMode.Always
        
        userNameTxtField.delegate = self
        pwdTxtField.delegate = self

            }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK:- Login Button
    
    
    @IBAction func loginBtn(sender: AnyObject)
        
    {
        userNameTxtField.resignFirstResponder()
        pwdTxtField.resignFirstResponder()
        
       
        if userNameTxtField.text == "" || pwdTxtField.text == ""
            
        {
            wrongpwdTxtField.hidden = false
            wrongUserTxtField.hidden = false
            
            
            
        }
        else
        {
            
            self.indicator.hidden = false
            indicator.startAnimating()
            indicator.frame = CGRectMake(((view.frame.width/2)-(indicator.frame.width/2)) , ((view.frame.height/2)-(indicator.frame.height/2)), indicator.frame.width, indicator.frame.height)
            
            
            var dataModel = NSData()
            
            
            
            var action = "login"
            var post = NSString(format:"username=%@&password=%@&action=%@",userNameTxtField.text,pwdTxtField.text,action)
            println(post)
            
            
            
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength = String(dataModel.length)
            
            var url = NSURL(string: "http://crowd.tools/api/loginapi.php?")
            
            var urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
                if Reachability.isConnectedToNetwork() == true
                {
                
                if (error != nil)
                {
                    println("\(error?.localizedDescription)")
                    
                }
                    
                else
                {
                    
                        var error:NSError?
                        
                        
                      var dicObj = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: &error) as? NSDictionary
                        
                        
                        
                        
                        
                        println(dicObj)
                        
                        
                        if (error != nil)
                        {
                            println("\(error?.localizedDescription)")
                            var alert = UIAlertView(title: "Alert", message: "Data Nil", delegate: self, cancelButtonTitle: "OK")
                            alert.show()
                            self.indicator.stopAnimating()
                            self.indicator.hidden = true

                        }
                        else
                        {
                            
                            
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                
                                
                                var success = dicObj?.valueForKey("status") as! NSString
                                
                                if success != "failure"
                                {
                                    let organizationId = dicObj?.valueForKey("organization_id") as! NSString
                                    
                                    println(organizationId)
                                    
                                    var userDefaults = NSUserDefaults.standardUserDefaults()
                                    
                                    
                                    userDefaults.setValue(organizationId, forKey: "orgId")
                                    
                                    self.indicator.stopAnimating()
                                    self.indicator.hidden = true
                                    

                                    
                                    let selectEvent = self.storyboard?.instantiateViewControllerWithIdentifier("selectEvent") as! SelectEventViewController
                                    
                                    self.navigationController?.pushViewController(selectEvent, animated: false)
                                    
                                    
                                }
                                else
                                    
                                {
                                    self.wrongpwdTxtField.hidden = false
                                    self.wrongUserTxtField.hidden = false
                                    self.indicator.stopAnimating()
                                    self.indicator.hidden = true
                                    
                                    
                                }
                                
                                
                                
                            })
                            
                        }
                        
                    
                    }
                
                }
                    
                else
                {
                    var alert = UIAlertView(title: "Alert", message: "Internet Connection Failed", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                    
                    
                    self.indicator.stopAnimating()
                    self.indicator.hidden = true
                    
                }

            })
            task.resume()
            
            
            
            
            
        }
        
        
    }
    //MARK: - TextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        wrongpwdTxtField.hidden = true
        wrongUserTxtField.hidden = true
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    //MARK:- Touch Method
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        userNameTxtField.resignFirstResponder()
        pwdTxtField.resignFirstResponder()
    }
    
}
