//
//  WelcomeTableViewCell.swift
//  Printer
//
//  Created by mrinal khullar on 10/5/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class WelcomeTableViewCell: UITableViewCell {

    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var lastNameLbl: UILabel!
    
    @IBOutlet var dateOfBirth: UILabel!
    
    @IBOutlet var checkedInTimeStamp: UILabel!
    
    
    @IBOutlet var userLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
